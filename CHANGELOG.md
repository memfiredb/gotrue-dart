## [0.0.1-dev.4]

- fix: email verification required on sign up

## [0.0.1-dev.3]

- chore: export Session and User classes

## [0.0.1-dev.2]

- fix: session and user parsing from json
- chore: method to get persistSessionString

## [0.0.1-dev.1]

- Initial pre-release.
